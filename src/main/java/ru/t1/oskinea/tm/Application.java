package ru.t1.oskinea.tm;

import ru.t1.oskinea.tm.api.controller.ICommandController;
import ru.t1.oskinea.tm.api.repository.ICommandRepository;
import ru.t1.oskinea.tm.api.service.ICommandService;
import ru.t1.oskinea.tm.component.Bootstrap;
import ru.t1.oskinea.tm.constant.ArgumentConst;
import ru.t1.oskinea.tm.constant.CommandConst;
import ru.t1.oskinea.tm.controller.CommandController;
import ru.t1.oskinea.tm.model.Command;
import ru.t1.oskinea.tm.repository.CommandRepository;
import ru.t1.oskinea.tm.service.CommandService;
import ru.t1.oskinea.tm.util.TerminalUtil;

import static ru.t1.oskinea.tm.util.FormatUtil.formatBytes;

public final class Application {

    public static void main(final String... args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
