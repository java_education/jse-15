package ru.t1.oskinea.tm.controller;

import ru.t1.oskinea.tm.api.controller.ICommandController;
import ru.t1.oskinea.tm.api.service.ICommandService;
import ru.t1.oskinea.tm.constant.ArgumentConst;
import ru.t1.oskinea.tm.constant.CommandConst;
import ru.t1.oskinea.tm.model.Command;

import static ru.t1.oskinea.tm.util.FormatUtil.formatBytes;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showErrorCommand() {
        System.out.println("[ERROR]");
        System.out.printf("Current program command are not correct. See '%s'\n", CommandConst.HELP);
    }

    @Override
    public void showErrorArgument() {
        System.out.println("[ERROR]");
        System.out.printf("Current program arguments are not correct. See 'task-manager.jar %s'\n", ArgumentConst.HELP);
        System.exit(1);
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.15.0");
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Evgeniy Oskin");
        System.out.println("E-mail: jizer@inbox.ru");
    }

    @Override
    public void showSystemInfo() {
        final int processorCount = Runtime.getRuntime().availableProcessors();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("[SYSTEM INFO]");
        System.out.println("Available processors: " + processorCount);
        System.out.println("Max memory: " + formatBytes(maxMemory));
        System.out.println("Total memory: " + formatBytes(totalMemory));
        System.out.println("Free memory: " + formatBytes(freeMemory));
        System.out.println("Used memory: " + formatBytes(usedMemory));
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        for (final Command command : commandService.getCommands()) System.out.println(command);
    }

}
